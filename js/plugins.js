// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());
/*Logo*/
function logoefec(){
    $('#logo, #logoa').removeClass().addClass("animated bounceInDown");
    window.setTimeout( function(){
        $('#logo, #logoa').removeClass()},
        1300
    );
}
$(document).ready(function(){
    // Imagenes que cargan aparecen con fadein
    $(".lazy").lazyload({
        effect : "fadeIn"
    });
    // Efecto en los logos css
    $('#logo, #logoa').addClass("animated bounceInDown");
    window.setTimeout( function(){
        $('#logo, #logoa').removeClass()},
        1300
    );
    $('#logo, #logoa').click(function(){
        logoefec();
    });
});
/*Fin Logo*/
/*Div Flotante*/
jQuery(function() {
    jQuery(window).scroll(function(){
        var scrollTop = jQuery(window).scrollTop();
        if(scrollTop >= 230){
            jQuery('#flotar').addClass("flotarr");
            jQuery('.logos').css({ display: "block" });
        }
        else{
            jQuery('#flotar').removeClass("flotarr");
            jQuery('.logos').css({ display: "none" });
        }
    });
});
/*Fin Flotante*/
//$("#xth").offset().top;
/*Div Form Flotante*/
(function($) {
    $("#sidebar").sticky({ topSpacing: 60, bottomSpacing: 600 });
})(jQuery);
/*Fin Form Flotante*/
/*slideshow*/
var slideshowSpeed = 6000;
var photos = [ {
        "title" : "Buen día, bienvenido a socabaya",
        "image" : "textura.png"
    }, {
        "title" : "Mensaje a socabaya",
        "image" : "vacation.jpg"
    }
];

$(document).ready(function() {
    var activeContainer = 1;
    var currentImg = 0;
    var animating = false;
    var navigate = function(direction) {
        if(animating) {
            return;
        }
        if(direction == "next") {
            currentImg++;
            if(currentImg == photos.length + 1) {
                currentImg = 1;
            }
        } else {
            currentImg--;
            if(currentImg == 0) {
                currentImg = photos.length;
            }
        }
        var currentContainer = activeContainer;
        if(activeContainer == 1) {
            activeContainer = 2;
        } else {
            activeContainer = 1;
        }
        showImage(photos[currentImg - 1], currentContainer, activeContainer);
    };
    var currentZindex = -1;
    var showImage = function(photoObject, currentContainer, activeContainer) {
        animating = true;
        currentZindex--;
        $("#headerimg" + activeContainer).css({
            "background-image" : "url(img/" + photoObject.image + ")",
            "display" : "block",
            "z-index" : currentZindex
        });
        $("#pictureduri")
            .html(photoObject.title);

        $("#headerimg" + currentContainer).fadeOut(function() {
            setTimeout(function() {
                animating = false;
            }, 500);
        });
    };
    // Start playing the animation
    interval = setInterval(function() {
        navigate("next");
    }, slideshowSpeed);
});